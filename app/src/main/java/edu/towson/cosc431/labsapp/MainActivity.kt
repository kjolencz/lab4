package edu.towson.cosc431.labsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import edu.towson.cosc431.labsapp.interfaces.ISongController
import edu.towson.cosc431.labsapp.interfaces.ISongRepository
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.song_view.*


class MainActivity : AppCompatActivity(), ISongController {
    override fun displaySong() {
        val song = songsRepository.getSong(currentSong)
        songName.text = song.name
        songArtist.text = song.artist
        songTrackNum.text = song.trackNumber.toString()
        isAwesomeCb.isChecked= song.isAwesome
    }

    override fun nextSong() {
        if(currentSong < songsRepository.getCount()-1){
            currentSong++
            displaySong()
        }
    }

    override fun previousSong() {
        if(currentSong > 0){
            currentSong--
            displaySong()
        }
    }

    override fun deleteSong() {
        songsRepository.deleteSong(currentSong)
        if(currentSong == songsRepository.getCount()) {
            currentSong--
        }
        if(songsRepository.getCount() > 0){
            displaySong()
        }else displayEmptyView()

    }


    private fun displayEmptyView() {
        songLayout.visibility = View.GONE
        emptyView.visibility = View.VISIBLE
    }

    override fun toggleAwesome() {
        val oldSong = songsRepository.getSong(currentSong)
        val newSong = oldSong.copy(isAwesome = !oldSong.isAwesome)
        songsRepository.replaceSong(currentSong, newSong)
        displaySong()
    }

    private var currentSong: Int = 0
    private lateinit var songsRepository: ISongRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        songsRepository = SongRepository()
        displaySong()

        buttonPrev.setOnClickListener { previousSong() }
        buttonNext.setOnClickListener { nextSong() }
        deleteButton.setOnClickListener { deleteSong() }
        isAwesomeCb.setOnClickListener{ toggleAwesome() }

    }

}
