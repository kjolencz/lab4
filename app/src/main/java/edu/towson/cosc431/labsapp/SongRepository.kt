package edu.towson.cosc431.labsapp

import edu.towson.cosc431.labsapp.interfaces.ISongRepository
import edu.towson.cosc431.labsapp.models.Song

class SongRepository: ISongRepository{


    private val songs: MutableList<Song> = mutableListOf()

    init{
        songs.addAll((0..10).map { idx -> Song("Songs${idx}", "Artist${idx}", "Album${idx}", idx, idx%2==0) })
    }

    override fun getCount(): Int {
        return songs.size
    }

    override fun getSong(idx: Int): Song {
       if(idx >= songs.size || idx < 0 ) throw Exception("Index Out Of Bounds")
        return songs[idx]
    }

    override fun getSongs(): List<Song> {
        return songs
    }

    override fun deleteSong(idx: Int) {
        if(idx >= songs.size || idx < 0 ) throw Exception("Index Out Of Bounds")
         songs.removeAt(idx)
    }

    override fun replaceSong(idx: Int, song: Song) {
        if(idx >= songs.size || idx < 0 ) throw Exception("Index Out Of Bounds")
         songs.set(idx,song)
    }

}