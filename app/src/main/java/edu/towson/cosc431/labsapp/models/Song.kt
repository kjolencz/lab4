package edu.towson.cosc431.labsapp.models

data class Song(
    val name : String,
    val artist: String,
    val album: String,
    val trackNumber: Int,
    val isAwesome: Boolean
)
